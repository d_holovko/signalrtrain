﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SignalRTrain.Hubs
{
    public class ViewHub : Hub
    {
        public void Play()
        {
            Clients.Others.play();
        }

        public void Pause()
        {
            Clients.Others.pause();
        }
    }
}