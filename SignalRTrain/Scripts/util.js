﻿var player = document.getElementById("videoPlayer");
player.controls = true;
player.loop = false;

let viewHub = $.connection.viewHub;

viewHub.client.play = function () {
    console.log("Play!");
    const playPromise = player.play();

    if (playPromise !== undefined) {
        playPromise.then(_ => {
            // Automatic playback started!
            // Show playing UI.
        }).catch(error => {
            console.log(error);
        });
    }

};

viewHub.client.pause = function () {
    console.log("Pause!");
    const playPromise = player.pause();

    if (playPromise !== undefined) {
        playPromise.then(_ => {
            // Automatic playback started!
            // Show playing UI.
        }).catch(error => {
            console.log(error);
        });
    }
};

$.connection.hub.start().done(function () {

    player.onplay = function () {
        viewHub.server.play();
    };

    player.onpause = function () {
        viewHub.server.pause();
    };

    /*var playButton = document.getElementById("play-button");
    playButton.addEventListener("click", function () {
        viewHub.server.play();
    });

    var pauseButton = document.getElementById("pause-button");
    pauseButton.addEventListener("click", function () {
        viewHub.server.pause();
    });*/
});